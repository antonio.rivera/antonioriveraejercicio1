public class Empleado extends Persona{
    private double salario;

    public Empleado(String nombre, String apellido, int edad, double salario) {
        super(nombre, apellido, edad);
        this.salario = salario;
    }

    public Empleado(double salario) {
        this.salario = salario;
    }

    public Empleado() {
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Empleado{"+
                "Nombre= "+ getNombre()+
                ", Apellido= "+ getApellido()+
                ", Edad= "+ getEdad()+
                ", salario=" + salario +
                '}';
    }
}
