import java.util.ArrayList;

public class Biblioteca {
    private String nombre;
    private String direcion;

    private ArrayList<Libro> libros = new ArrayList<>();

    private ArrayList<Empleado> empleados = new ArrayList<>();

    public Biblioteca(String nombre, String direcion, ArrayList<Libro> libros, ArrayList<Empleado> empleados) {
        this.nombre = nombre;
        this.direcion = direcion;
        this.libros = libros;
        this.empleados = empleados;
    }

    public Biblioteca() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirecion() {
        return direcion;
    }

    public void setDirecion(String direcion) {
        this.direcion = direcion;
    }

    public ArrayList<Libro> getLibros() {
        return libros;
    }

    public void setLibros(ArrayList<Libro> libros) {
        this.libros = libros;
    }

    public ArrayList<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(ArrayList<Empleado> empleados) {
        this.empleados = empleados;
    }
    
    public void encontrarEmpleado(String nombre){
        // System.out.println("Prueba "+empleados.stream().filter(e -> e.getNombre()==nombre).findAny().isEmpty());
                if (empleados.stream().filter(e -> e.getNombre()==nombre).findAny().isEmpty())
                    System.out.println("No se ha encontrado ningun usuario con el nombre: " + nombre);
                 else
                    empleados.stream().filter(e -> e.getNombre()==nombre).forEach(System.out::println);
    }

    public void encontrarLibro(String titulo){
        if(libros.stream().filter(l -> l.getTitulo()==titulo).count()>1)
            System.out.println("Error: Solo puede existir un solo libro con el mismo título.");
        else if (libros.stream().filter(l->l.getTitulo()==titulo).findAny().isEmpty())
            System.out.println("No se ha encontrado ningun libro con el título: "+titulo);
        else
            libros.stream().filter(l -> l.getTitulo()==titulo).forEach(System.out::println);
    }

    public void calcularGastosSalarios(){
        System.out.println("Gastos de salarios: "+empleados.stream().mapToDouble(e->e.getSalario()).sum());
    }

    @Override
    public String toString() {
        return "Biblioteca {" +
                " nombre='" + nombre + '\'' +
                ", direcion='" + direcion + '\'' +
                ", libros=" + libros +
                ", empleados=" + empleados +
                '}';
    }

}
