import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Empleado empleado = new Empleado();
        Empleado empleado1=new Empleado();
        Libro libro = new Libro();
        Libro libro1 = new Libro();
        Autor autor = new Autor();
        Biblioteca biblioteca = new Biblioteca();
        ArrayList<Libro> libros = new ArrayList<>();
        ArrayList<Empleado> empleados = new ArrayList<>();

        empleado.setNombre("Antonio");
        empleado.setApellido("Rivera");
        empleado.setEdad(23);
        empleado.setSalario(500);

        empleado1.setNombre("Antonio");
        empleado1.setApellido("Hidalgo");
        empleado1.setEdad(24);
        empleado1.setSalario(1500);
        empleados.add(empleado);
        empleados.add(empleado1);

        autor.setNombre("Miguel");
        autor.setApellido("Delibes");
        autor.setEdad(89);
        autor.setGeneracion("Generacion del 36");

        libro.setAutor(autor);
        libro.setTitulo("El Camino");
        libro.setAnioPublicacion(1950);

        /*libro1.setAutor(autor);
        libro1.setTitulo("El Camino");
        libro1.setAnioPublicacion(1960);*/

        libros.add(libro);
        libros.add(libro1);

        biblioteca.setNombre("Biblioteca de Jaén");
        biblioteca.setDirecion("Calle en Jaén nº 1");
        biblioteca.setLibros(libros);
        biblioteca.setEmpleados(empleados);




        System.out.println("Persona empleado: " + empleado.getNombre()+" "+empleado.getApellido()+" "+empleado.getEdad());
        System.out.println("Libro: "+libro.getTitulo()+" "+libro.getAutor() + " " + libro.getAnioPublicacion());

        System.out.println(biblioteca);
        biblioteca.encontrarEmpleado("Antonio");
        biblioteca.encontrarLibro("El Camino");
        biblioteca.calcularGastosSalarios();
    }
}