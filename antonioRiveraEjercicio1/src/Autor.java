public class Autor extends Persona{
    private String generacion;

    public Autor(String nombre, String apellido, int edad, String generacion) {
        super(nombre, apellido, edad);
        this.generacion = generacion;
    }

    public Autor(String generacion) {
        this.generacion = generacion;
    }

    public Autor() {

    }

    public String getGeneracion() {
        return generacion;
    }

    public void setGeneracion(String generacion) {
        this.generacion = generacion;
    }

    @Override
    public String toString() {
        return "Autor {" +
                "Nombre="+getNombre()+
                "Apellido="+getApellido()+
                "Edad="+getEdad()+
                "generacion='" + generacion + '\'' +
                '}';
    }
}
